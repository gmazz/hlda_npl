%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% University/School Laboratory Report
% LaTeX Template
% Version 3.0 (4/2/13)
%
%
% 
% Original author:
%
% Cezary Dendek
% Giovanni Mazzocco 
% 
%
% License:
% CC BY-NC-SA 3.0 (http://creativecommons.org/licenses/by-nc-sa/3.0/)
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%----------------------------------------------------------------------------------------
%	PACKAGES AND DOCUMENT CONFIGURATIONS
%----------------------------------------------------------------------------------------

\documentclass{article}

\usepackage{mhchem} % Package for chemical equation typesetting
\usepackage{siunitx} % Provides the \SI{}{} command for typesetting SI units
%\usepackage[]{algorithm2e}
\usepackage{algorithm}
\usepackage{algpseudocode}
\usepackage{graphicx} % Required for the inclusion of images
\usepackage{multicol}
\usepackage{tabularx}

\setlength\parindent{0pt} % Removes all indentation from paragraphs

\renewcommand{\labelenumi}{\alph{enumi}.} % Make numbering in the enumerate environment by letter rather than number (e.g. section 6)

%\usepackage{times} % Uncomment to use the Times New Roman font

%----------------------------------------------------------------------------------------
%	DOCUMENT INFORMATION
%----------------------------------------------------------------------------------------

\title
{Document classification via \\ Hierarchical Supervised Latent Dirichlet Allocation} % Title

\author{Cezary Dendek \\ Giovanni Mazzocco} % Author name

\date{\today} % Date for the report

\begin{document}

\maketitle % Insert the title, author and date

%\begin{center}
%\begin{tabular}{l r}
%Report date: & June 14, 2014 \\ % Date the experiment was performed 
%\end{tabular}
%\end{center}

% If you wish to include an abstract, uncomment the lines below
 \begin{abstract}

The classification of unstructured data that going under the general name of \emph{document classification} is a central topic in library science, information science and computer science. The task consists in assigning a document to one or more classes or categories. This procedure can be performed either manually or algorithmically.  In this paper we introduce an implementation of the Hierarchically Supervised Latent Dirichlet Allocation (HSLDA): a model for hierarchically and multiply labeled bag-of-word data. The model has been trained and tested on a dataset based on Wikipedia documents  and can be used on every hierarchically structured dataset.

 \end{abstract}


%----------------------------------------------------------------------------------------
%	SECTION 1
%----------------------------------------------------------------------------------------

\section{Introduction}

Motivated by practical applications such as spam filtering, e-mail routing, news filtering, language identification, sentiment analysis, etc. document classification techniques have been extensively developed over the last decade. Although lot of progresses have been already achieved on this field, the discrimination between labeled document is still a difficult task, where substantial human involvement is ofter required before a tolerable level  of system accuracy is attained. Hence the development of new machine learning tools, able to better address this issue lowering the amount of human involvement in data preprocessing and analysis, is indeed a matter of great interest.

In this work we propose a Hierarchically Supervised Latent Dirichlet Allocation (HSLDA) model inspired by Perrotte et.al. in 2011 [1]. The implemented model has being trained and tested on documents obtained from Wikipedia, but is evenly applicable to other data, namely, any unstructured representations of data that have been hierarchically classified (e.g., image catalogs with bag-of-feature representations, etc.).
There are several challenges entailed in incorporating a hierarchy of labels into the model. Given a large set of potential labels, each instance has only a small number of labels associated to it. Moreover, naturally negative labels are often missing within the data, and the absence of a label can't be automatically interpreted as a negative labeling.
The applied approach either uses the nested topics structure while exploiting the hierarchical structure of the labels. The model implemented is an extended version of supervised Latent Dirichlet Allocation (sLDA) [2] that take advantage of hierarchical supervision.
An efficient way to incorporate hierarchical information into the sLDA model is here proposed. The principal hypotheses is that the context of labels within the hierarchy provides valuable information about labeling.
The main variant introduced in our version with respect to the original HSLDA by Perrotte et al. consists in a different sampling procedure as we use sampling from normal distribution upon covariant matrix calculation instead of random sampling.

%\begin{figure}
%\begin{center}
%\includegraphics[width=0.75\textheight]{WF_scheme} % Include the image placeholder.png
%\caption{The framework is divided into three major parts: i) NP enrichment, were the NIP data are filtered, ii) Domain feature selection for classification purposes, iii) Statistical analysis of the data and classification training/testing}
%\end{center}
%\end{figure}

%----------------------------------------------------------------------------------------
%	SECTION 2
%----------------------------------------------------------------------------------------

\section{Data}
%\subsection{description}

\subsection{Original data}
The original data are publicly available on KAGGLE [3] and have been obtained from the 4th edition of the Large Scale Hierarchical Text Classification (LSHTC) Challenge: a hierarchical text classification competition, using very large datasets. 
The data consists in multi-label classification pre-processed documents based on Wikipedia documents. The hierarchy is a graph with potentially several cycles. The number of categories is roughly 325,000 and the number of documents is 2,400,000. A document can appear in multiple classes. The original data consists in three different input files:\\

%\begin{tabular}
	\begin{description}
	\item[Hierarchy.txt] \hfill \\
	 
	File containing a numerical pair that given a category $k_{i}$
	defines all the $par(k_{i})\to k_{i}$ relations.\\\\
	Example:\\
	$\begin{matrix}
  	55 & 123 \\
  	...
 	\end{matrix}$\\\\
	In the case above  $k_{55} = par(k_{123})$, etc. \\
	The hierarchy file contains 863261 pairs (file dim. 13MB)\\\\\\\\\\
	
	
	\item[Train.csv \& Test.csv] \hfill \\
	
	Training and testing data are formatted in libSVM format: \\\\
	label, label, ..., label    feat:value,  feat:value, ..., feat:value \\\\
	Where each vector represents a document which can potentially belong to several categories. \\\\
	\textsl{label}: integer that defines the category to which a document belongs. \\
	\textsl{feat}: integer representing a term. \\
	\textsl{value}: double representing the weight of the ith term in the document.\\\\
	Example:\\
	
	$\begin{matrix}
  	81, & 55 &8:1 &18:2 \\
  	...
 	\end{matrix}$\\
	
	The example shows a document containing the categories $k_{81}$ and $k_{55}$
	and the terms nb. 8 and 18  with weights 1 and 2 respectively, etc.\\
	The training and test files contains 2365437(~958MB) and 452168(~180MB) documents respectively.
		 
\end{description}

\subsection{Pre-processing steps}

The principal steps of the pre-processing phase are:\\

I) Sampling \\
II) Grouping all parent, children and documents for each given category\\
III) Find all the ancestor of a give category\\
IV) Generate the collapsed matrix\\
V) Compute the covariance of the collapsed matrix\\\\\\

\subsection{Sampling}

The data in input have been initially sampled by each category $k_{i}$ via random walk on a dataset including all $par(k_{i})$ and $ch({k_{i}})$ (parent and children for a given $k$).
The data dimensional reduction is approximately:\\   
$|Data_{out}| = 0.625 \cdot |Data_{in}|$

\subsection{Find ancestors}

While either parents, children and documents of a category are easy to group, in order to find all the ancestor of a given category a non trivial algorithm should be adopted. We used the properties of the adjacency matrix $A$ of the graph. In order to find all the ancestors of a given category $anc(k)$ within the entire graph, the dot product of the $A$ with itself is computed until convergence. The A matrix eventually returned contains all the ancestor of a give category $k_{i}$.

\begin{algorithm}
%\caption{Matrix dot}\label{MM}
\begin{algorithmic}[1]
\Procedure{Matrix Dot}{$A$}\Comment {$A_{0}$: initial adjacency matrix}
   \State $A_{0}\gets A$
   \State $i\gets 0$
   \While 
   {$||sum(abs(A_{i-1}-A_{i}))||\not=0$}  \Comment{$A$ is returned when stable}
      \State $A_{i+1}\gets sgn(A_{i} \cdot A)$
   	\State $i\gets i+1$
   \EndWhile
\EndProcedure
\end{algorithmic}
\end{algorithm}

\begin{multicols}{2}

	$A = \begin{pmatrix}
		a_{1,1} & a_{1,2} & \cdots & a_{1,n} \\
		a_{2,1} & a_{2,2} & \cdots & a_{2,n} \\
		\vdots  & \vdots  & \ddots & \vdots  \\
		a_{n,1} & a_{n,2} & \cdots & a_{n,n}
	\end{pmatrix}$

\columnbreak

\[ \left\{ 
  \begin{array}{ll}
    a_{i,j} = 1 \quad \text{if $k_{j}$ is $arc(k_{i})$}\\
    a_{i,j} = 0 \quad \text{otherwise}
  \end{array} \right.\]

\end{multicols}
Matrix $A$ representing the child-ancestor relations between each given category $k$ and all the other categories belonging to the entire category graph.

\subsection{Collapsed Matrix \& Covariance calculation}
Since each document $d$ has an associated array of categories $c_{d}$,  we observe:\\

$\forall d \in D, \quad  c_{d} = \{k_{1}, k_{2}, ..., k_{n }\}_{d} $ \\
$\forall k \in K, \quad  anc(k) $\\
$\forall d \in D, \quad  b_{d} := anc(k_{1}) \vee anc(k_{2}) \vee  ... \vee anc(k_{n}) $\\

Where the $D$ is the set of all documents, $K$ the set of all categories and $anc(k)$ represents all the ancestors of the category $k$.\\
Hence we define $b_{d}$ as the vector generated computing the boolean sum of all the the ancestors of the categories associated to a given document $d$. In other words for each given document $d$, we can obtain a "collapsed" vector $b_{d}$, containing all the joint ancestors of all the categories. Repeating this operation foreach document, we'll eventually obtain a matrix B containing all the collapsed vectors for all documents
Then the covariance of the matrix B, has been thus computed and indicated as $\Sigma$.


%----------------------------------------------------------------------------------------
%	SECTION 3
%----------------------------------------------------------------------------------------
\section{The model}
Parameters: 
\begin{multicols}{2}

	$\mu \sim U[-2,2]$\\
	$\sigma \sim \Gamma(3,2)$\\
	$\eta \sim N(\mu, \sigma^2)$\\
	$\sigma_{err} \sim \Gamma(3,12)$\\
	
\columnbreak
	$
	\forall \eta \cdot \theta_{m} \sim N(\hat{z}, \hat{\Sigma} + I\cdot0.05) \\
	\qquad \forall p_{d} =  \Phi_{0,\sigma_{err}}(\eta \cdot \theta_{m}) \\
	labelled(d,l) \sim Bernulli(p_{d}[C]) \\
	$
\end{multicols}	
	
The model consists of two main parts: Latent Dirichlet Allocation (LDA) and Correlated Probit Regression (CPR). LDA provides us with the model of latent topics, capable of affecting the CPR by providing the matrix $\theta$. The next step consists in mapping the latent topics to our categories.
This operation can be performed by sampling from the \emph{correlated probit} $p_{d}$: our personal variant of the probit regression. This modification allows us to model the dependencies between categories using their correlation matrix $\Sigma$ as described in 2.5.
The idea behind our variant is simple but powerful: combine the multivariate normal approximation of the categories with a probit transformation, in order to obtain the labelling probabilities.
In our model the matrix $\Sigma$ is regularized, thus we use $\Sigma + I\cdot0.05$ where  I is an identity matrix. It is important to notice, that $\eta$ is also a matrix.

%----------------------------------------------------------------------------------------
%	SECTION 4
%----------------------------------------------------------------------------------------

\section{Materials \& Methods}

The pipeline of the implementation was written in Python, while the model was implemented in STAN: a probabilistic programming language implementing statistical inference using either Markov chain Monte Carlo sampling (as in our case) or Optimization-based point estimation [4].
The following Python libraries have been extensively used for calculation: Cython, Numpy, Scipy, Pystan and Scikit-Learn.


%----------------------------------------------------------------------------------------
%	SECTION 5
%----------------------------------------------------------------------------------------
\section{Results \& Discussion}

% Table example:

\begin{tabular}{ |p{2cm}|p{1.3cm}|p{1.3cm}|p{1.3cm}|p{1.3cm}|p{1.7cm}|p{1.3cm}|}
 \hline
  Sample size & MSE & MC(\%) &Precision& Recall & F-measure & support\\
 \hline
 2000 &0.0739 &0.1981 &0.6946 &0.5833 & 0.6341 &156\\
 8000 &0.0272 &0.075 &0.8717 &0.8717 & 0.8717 &156\\

 \hline
\end{tabular}
\\\\\\
The classification performances are shown in the table above.
The experiments have been performed upon 10-fold cross validation, in order to exclude overfitting  
and give a realistic estimation of the predictive capability of our model if applied to external test sets.
As aspected, the productivity of the model increase with the size of the training sample.\\
The smallest F-measures scored by our method (0.6341) is far superior to the maximum F-measure reported by the winner of the LSHTC Challenge (maximum macro-avaraged F-score = 0.34) which was obtained by simple Naive Bayes classification [5]. 
This observation suggest the concrete applicability of our model to a vast set of document classification problems were hierarchical informations are available.


%----------------------------------------------------------------------------------------
%	SECTION 6
%----------------------------------------------------------------------------------------
\section{Conclusions}

We introduced and implemented a new Hierarchically Supervised Latent Dirichlet Allocation model
able to efficiently exploit the hierarchical information present in several structured and unstructured documents. We demonstrated the validity of our approach on a very large hierarchical dataset, suggesting a scenario of possible application of our model in the context of document classification.  
We expect to see an improvement of the prediction capabilities of our method, prior to further application of optimization procedures.

%----------------------------------------------------------------------------------------
%	BIBLIOGRAPHY (example)
%----------------------------------------------------------------------------------------

\begin{thebibliography}{9}

\bibitem{lamport94}
  A. J. Perrotte, N. Bartlett, N. Elhadad, F. Wood.
  \emph{Hierarchically Supervised Latent Dirichlet Allocation},
Advances in Neural Information Processing Systems 24 (2011) 

\bibitem{lamport94}
D. Blei and J. McAuliffe.  
 \emph{ Supervised topic models.},
Advances in Neural Information Processing Systems 20: 121-128 (2008)

\bibitem{lamport94}
http://www.kaggle.com/c/lshtc

\bibitem{lamport94}
http://mc-stan.org

\bibitem{lamport94}
http://bit.ly/1uwBnHu

\end{thebibliography}


%----------------------------------------------------------------------------------------


\end{document}