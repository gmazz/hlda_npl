## based on 

data {
  int<lower=2> K; // num topics
  int<lower=1> M; // num docs
  int<lower=1> L; // number of categories
  matrix<lower=-1,upper=1>[M,L] catAssign;
  int<lower=0,upper=1> catAssignInt[M,L];
  matrix[L,L] Sigma;
  matrix[M,K] theta;
  vector<lower=-1,upper=1>[L] empiricalLabels;
}
parameters {
  matrix[L,K] eta;
  real<lower=-2,upper=2> mu;
  real<lower=0.001> sigma;
  real<lower=0.001> sigmaerr;
  #vector[L] intercept;
}
model {
		#Step 2: for each label draw a label application coefficient
		#mu ~ normal(0,2);
		sigma ~ gamma(3,2);
		for (lab in 1:L){
			eta[lab] ~ normal(mu,sigma);
		}
		#intercept ~ normal(0, 1);
		sigmaerr ~ gamma(3,12);
		
		
		#Step 4: for each document:
			# draw topic proportions
	       for (m in 1:M){
	       	 vector[L] prob;
	       	 vector[L] vals;
	       	 eta*(theta[m])' ~ multi_normal(empiricalLabels,Sigma);
    	     ## Variant 1
    	     #(catAssign[m])' ~ normal(eta*(theta[m])',sigmaerr2); 
    	     ## Variant 2
    	     vals <- eta*(theta[m])';
    	     for (lab in 1:L){
    	     	prob[lab] <- normal_cdf(vals[lab],empiricalLabels[lab],sigmaerr);
    	     	#prob[lab] <- normal_cdf(vals[lab],0,sigmaerr);
    	     	catAssignInt[m,lab] ~ bernoulli(prob[lab]);
    	     }    	     
    	     
    	   }
         
         
		 
}