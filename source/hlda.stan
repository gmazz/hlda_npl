## based on 

data {
  int<lower=2> K; // num topics
  int<lower=2> V; // num words (vocabulary size)
  int<lower=1> M; // num docs
  int<lower=1> N; // total word instances
  int<lower=1> L; // number of categories
  int<lower=1,upper=V> w[N]; // word n 
  	//(concatenation of all documents with particular words indicated as int 1..V
  int<lower=1,upper=M> doc[N];  // doc ID for word n
  	//(concatenation of all documents with particular document indicated as int 1..M
  vector<lower=0>[K] topicsprior;     // topic prior ## TODO: convert to single elem 
  vector<lower=0>[V] wordsprior;      // word prior
  matrix<lower=-1,upper=1>[M,L] catAssign;
  matrix[L,L] Sigma;
  real<lower=0> alpha;
}
parameters {
  simplex[K] theta[M]; // topic dist for doc m
  simplex[V] phi[K]; // word dist for topic k  ## point 1
  matrix[L,K] eta;
  real<lower=-2,upper=2> mu;
  real<lower=0,upper=4> sigma;
  simplex[K] betanew;
}
model {
		#Step 1: for each topic draw a distribution over words
		for (k in 1:K)
			phi[k] ~ dirichlet(wordsprior);     // prior ## point 1
		
		#Step 2: for each label draw a label application coefficient
		for (lab in 1:L){
			eta[lab] ~ normal(mu,sigma);
		}
		
		
		#Step 3: draw the global topic proportions
		betanew ~ dirichlet(topicsprior);
			
		#Step 4: for each document:
			# draw topic proportions
	       for (m in 1:M){
    	     theta[m] ~ dirichlet(alpha*betanew);  // prior
    	     #theta[m] ~ dirichlet(topicsprior);  // prior
    	     (catAssign[m])' ~ multi_normal(eta*theta[m],Sigma);
    	   }
         
         
       for (n in 1:N) {
         real gamma[K];
         for (k in 1:K){
           gamma[k] <- log(theta[doc[n],k]) + log(phi[k,w[n]]);
         }
         increment_log_prob(log_sum_exp(gamma));  // likelihood
		}
	
		 
}