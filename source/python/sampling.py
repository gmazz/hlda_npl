import random

def rws(catIdx, randIt): #Params passed in model.py

    keys = catIdx.catId2Obj.keys()
    node = catIdx.getCategory(random.sample(keys, 1)[0])

    randSel=[]
    randSel.append(node)
    check = {}
    check[node.catId]=1
    tmp_val = 0

    for k in range(1,randIt):
        selSubset=node.children+node.parents
        node = random.sample(selSubset, 1)[0]

        if node in randSel:
            tmp_val = check.get(node.catId)
            tmp_val += 1
            check.update({node.catId:tmp_val})

        else:
            check.update({node.catId:1})
        randSel.append(node)

    redSel = set(randSel)
    return redSel, check


class DataSampler:
    def __init__(self, dataStore, randomSteps=30):
        self.dataStore=dataStore
        self.randomSteps=randomSteps
        self.sampledCat=None
        self.sampledCatCounter=None
        pass
    
    #reduced selection of the nodes (some of them are visited more that 1 time during random walk)
    def perform(self):
        redSel, check = rws(self.dataStore.catIdx, self.randomSteps)
        self.sampledCat=redSel
        self.sampledCatCounter=check
        pass
    
    def getSample(self):
        return self.sampledCat
        
        