from sklearn import cross_validation
from sklearn.metrics import precision_recall_fscore_support

class Indexer:
    def __init__(self):
        self.mapping={}
        self.counter = 0
        
    def add(self,iterableColl):
        values=[]
        for elem in iterableColl:
            val=self.mapping.get(elem)
            if val==None:
                val=self.counter
                self.mapping[elem]=val
                self.counter=self.counter+1
            values.append(val)
        return values
    
    def addValue(self,valToMap):
        val=self.mapping.get(valToMap)
        if val==None:
            val=self.counter
            self.mapping[valToMap]=val
            self.counter=self.counter+1
        return val
    
    def getInvertedMapping(self):
        return dict([[v,k] for k,v in self.mapping.items()])
        
    def getNumber(self):
        return self.counter

def KFCrossVal(corpus_len,k):
        KFCV = cross_validation.KFold(corpus_len, k, indices=True, shuffle=True)
        return KFCV

def Score(y_true,y_pred):
        res = precision_recall_fscore_support(y_true, y_pred, average='macro')
        return res