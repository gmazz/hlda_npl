"""
Recursive implementation of DeepFirstSearch algorithm.

"""

def rDFS(anc_list, visited, v):
    if v in visited:
        return anc_list[v]
    anc_list[v] = []
    visited.add(v)
    anc = list(v.parents)
    for p in v.parents:
        anc += rDFS(anc_list, visited, p)
    anc_list[v] = anc
    return anc

def main(all):
    visited = set()
    anc_list = {}
    for v in all:
        rDFS(anc_list, visited, v)
    return anc_list

