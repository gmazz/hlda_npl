from matrix_gen import mainMD
import numpy as np



class CategoryIndex:

    def __init__(self):
        self.catId2Obj={}
        self.docsSet=set([])
        self.adjMatrix=None
        self.adjMatrixCollapsed=None
        self.adjMatrixIdx=None

        pass

    def addPair(self,parentId,childId):

        parentObj=self.catId2Obj.get(parentId)
        if parentObj==None:
            parentObj=Category(parentId)
            self.catId2Obj[parentId]=parentObj
            pass

        childObj=self.catId2Obj.get(childId)
        if childObj==None:
            childObj=Category(childId)
            self.catId2Obj[childId]=childObj
            pass

        parentObj.addChild(childObj)

    def addDocument(self,doc,parentId):
        self.catId2Obj[parentId].addDocument(doc)
        self.docsSet.add(doc)
        
    def hasMultipleParentCategories(self):
        for catId,catObj in self.catId2Obj.items():
            if len(catObj.parents)>1:
                return True
        return False

    def processTotalDocumentCount(self):
        openSet=self.catId2Obj.values()
        while len(openSet)>0:
            nextObj=openSet.pop()            
            pass
        pass

    def getCategory(self,catId):
        return self.catId2Obj.get(catId)
    
    def restrictToSample(self, iterableOfCat):
        newCat=CategoryIndex()
        catSet=set(iterableOfCat)
        allDocs=set([])
        for cat in iterableOfCat:
            catId=cat.catId
            children=set(cat.children).intersection(catSet)
            parents=set(cat.parents).intersection(catSet)
            for parent in parents:
                newCat.addPair(parent.catId, catId)
            for child in children:
                newCat.addPair(catId,child.catId)
            allDocs.update(cat.documents)
        for docOrg in allDocs:
            doc=Document(docOrg.filePos)
            docParents=set(docOrg.parents).intersection(catSet)
            for parent in docParents: 
                newCat.addDocument(doc, parent.catId)
            pass
        return newCat
        pass
    
    def prepareMatrices(self):
        originalMat, collapsedMembership, objMap = mainMD(set(self.catId2Obj.values()))
        self.adjMatrix=originalMat
        self.adjMatrixCollapsed=collapsedMembership
        #print collapsedMembership
        self.adjMatrixIdx=objMap
        pass
    
    def inferFullDocsMembership(self):
        for doc in self.docsSet:
            '''getting cat. membership'''
            currvec=self.adjMatrixCollapsed[self.adjMatrixIdx[doc.parents[0]],:]
            if len(doc.parents)>1:
                for vecit in range(1,len(doc.parents)-1):
                    currvec=currvec+self.adjMatrixCollapsed[self.adjMatrixIdx[doc.parents[vecit]],:]
            doc.membership=np.sign(currvec)

class Category:

    def __init__(self, catId):
        self.catId=catId
        self.children=[]
        self.parents=[]
        self.documents=[]
        pass

    def addChild(self, childObj):
        self.children.append(childObj)
        childObj.parents.append(self)
        pass

    def addDocument(self,doc):
        self.documents.append(doc)
        doc.parents.append(self)
        pass

    def getDocumentCount(self):
        return len(self.documents)
    
    
class Document:

    def __init__(self,filePos):
        self.filePos=filePos
        self.parents=[]
        pass





