from model import *
from dataLoad import *
from sampling import *
from matrix_gen import mainMD
import cPickle as pickle
import os.path
import inference

def getCacheName(rstep, filePrefix="./../../data/sampleCache"):
    return filePrefix+str(rstep)+".pickle"

def ensureCache(randomStepsList, filePrefix="./../../data/sampleCache",forceOverwrite=False):
    toCompute=[]
    if forceOverwrite:
        toCompute=randomStepsList
    else:
        for rstep in randomStepsList:
            if not os.path.isfile(getCacheName(rstep, filePrefix)):
                toCompute.append(rstep)
                pass
            pass   
    if len(toCompute)==0:
        print "All files are present"
        return
    dataload = DataLoader()
    dataload.perform()
    for rstep in toCompute:
        print "Sampling with "+str(rstep)+" steps"
        sampleCacheFile=getCacheName(rstep, filePrefix)
        datasampler = DataSampler(dataload, randomSteps=rstep)
        datasampler.perform()
        
        sample=datasampler.getSample()
        ## we can forget about old data store
        dataload2=dataload.restrictToSample(sample)
        dataload2.loadDocsFeatures()
        dataload2.finalizeLoad()
        pickle.dump( dataload2, open( sampleCacheFile, "wb" ) )
        print "Results saved in "+sampleCacheFile+"."
        pass
    dataload.finalizeLoad()
    pass

#ensureCache([30,60,120,240,480,960])


def performTraining(sample_file, mcmcIt, ldaTopics, output_file, cvFold):
    print "Loading the sample"
    dataload=pickle.load( open( sample_file, "rb" ) )
    print "Preparing adjacency matrices"
    dataload.catIdx.prepareMatrices()
    dataload.catIdx.inferFullDocsMembership()
    print "Building global dictionary"
    dataload.buildDictionary()    
    print "Performing inference:"
    if cvFold==1:
        inference.performFastLDA(dataload, mcmcIt, ldaTopics, output_file)
    else:
        inference.performFastLDACV(dataload, mcmcIt, ldaTopics,cvFold)

    
    pass




if __name__ == "__main__":

    dataload = None
    randomSteps=30
    #randomSteps=30
    ##randomSteps=960
    
    sampleCacheFile="./../../data/sampleCache"+str(randomSteps)+".pickle"
    if not os.path.isfile(sampleCacheFile):
        dataload = DataLoader()
        dataload.perform()
        
        datasampler = DataSampler(dataload, randomSteps=randomSteps)
        datasampler.perform()
        
        sample=datasampler.getSample()
        ## we can forget about old data store
        dataload=dataload.restrictToSample(sample)
        dataload.loadDocsFeatures()
        dataload.finalizeLoad()
        pickle.dump( dataload, open( sampleCacheFile, "wb" ) )
    else:
        dataload=pickle.load( open( sampleCacheFile, "rb" ) )
    
    dataload.catIdx.prepareMatrices()
    dataload.catIdx.inferFullDocsMembership()
    
    # establishing the dictionary
    dataload.buildDictionary()
    
    ## data export
    
    # inferred categories
    
    #testLDA(dataload)
    
    
    #testHLDA(dataload)
    #testRes()
    
    inference.testFastLDA(dataload)
    
    



    

