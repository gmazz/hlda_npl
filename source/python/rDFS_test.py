"""
Recursive implementation of DeepFirstSearch algorithm.

"""

def find_obj(catId, all):
    for p in all:
        if p.catId == catId:
            return p

def rDFS(anc_list, visited, v):
    if v in visited:
        return anc_list[v.catId]
    anc_list[v.catId] = []
    visited.add(v)
    anc = list(v.parents)
    for pid in v.parents:
        p = find_obj(catId, all)
        if p:
            anc += rDFS(anc_list, visited, p)
    anc_list[v.catId] = anc
    return anc

def main(all):
    visited = set()
    anc_list = {}
    for v in all:
        rDFS(anc_list, visited, v)
    return anc_list


######################### Class for testing purposes ######################

class Obj():
    def __init__(self, catId, parents):
        self.catId = catId
        self.parents = parents

a = Obj(1, [2,3,4,5])
b = Obj(4, [1,6,7,8])
c = Obj(6, [12,13,15])
d = Obj(13, [20,22,25])
e = Obj(20, [31,26,44])
f = Obj(7, [45,34,28])

all = [a,b,c,d,e,f]
#anc_list = main(all)