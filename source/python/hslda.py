import argparse
import executor
import os

def samplerCaller(args):
    executor.ensureCache(randomStepsList=args.stepNo, 
                         filePrefix=args.outfile_prefix, 
                         forceOverwrite=(not args.lazySampler))
    pass

def trainCaller(args):
    #validation
    if not os.path.isfile(args.sample_file):
        print "ERROR: the file "+args.sample_file+" does not exist."
        return
    if args.hidden_topics!=None and args.hidden_topics<2:
        print "ERROR: the hidden_topics needs to be at least 2."
        return
    if args.sampling_iterator!=None and args.sampling_iterator<50:
        print "ERROR: the sampling_iterator needs to be at least 50."
        return
    if os.path.isfile(args.output_model_file):
        print "ERROR: the file "+args.output_model_file+" already exists."
        return
    cvFolds=vars(args).get("N")
    if cvFolds!=None and cvFolds<2:
        print "ERROR: the number of crossvalidation folds (N) needs to be at least 2."
        return
    if cvFolds==None:
        args.cvFolds=1
    else:
        args.cvFolds=args.N
    
    executor.performTraining(sample_file=args.sample_file,
                             mcmcIt=args.sampling_iterator,
                             ldaTopics=args.hidden_topics,
                             output_file=args.output_model_file,
                             cvFold=args.cvFolds
                             )
    pass
    
progdescription='Hierarchical Supervised Latent Dirichlet Allocation'

#commands = ['corpora','sample', 'train', 'apply']

parser = argparse.ArgumentParser(description=progdescription)
subparsers = parser.add_subparsers(title='operations',description='valid operations',help='sub-command help')

#parser_sample = subparsers.add_parser('corpora', help='corpora manager')
#parser_sample.add_argument('outfile_prefix', help='the prefix of the output file(s)')


#parser.add_argument('operation', choices=commands,
#                   help='the operation to perform')
parser_sample = subparsers.add_parser('sample', help='random walk sampler of documents and labels')
parser_sample.add_argument('outfile_prefix', help='the prefix of the output file(s)')
parser_sample.add_argument('--lazy', dest="lazySampler", help='do not overwrite output files', action="store_true", default=False)
parser_sample.add_argument('stepNo', type=int, help='number of random steps to perform (e.g. 60)',nargs="+")
parser_sample.set_defaults(handler=samplerCaller)

parser_train = subparsers.add_parser('train', help='training of the HSLDA model')
parser_train.add_argument('sample_file', help='path to sample file')
parser_train.add_argument('--cv', dest="N", help='perform N-fold crossvalidation', type=int)
parser_train.add_argument('hidden_topics', type=int, help='number of LDA hidden topics', default=None)
parser_train.add_argument('sampling_iterator', type=int, help='number of MCMC sampling iterations (e.g. 6000)', default=6000)
parser_train.add_argument('output_model_file', help='path to output file with a model')
parser_train.set_defaults(handler=trainCaller)


args = parser.parse_args()
args.handler(args)


'''
python2.7 hslda.py sample --lazy ./../../data/sampleCache 30 60
python2.7 hslda.py train ./../../data/sampleCache30.pickle 30 100 ./../../data/out30
python2.7 hslda.py train --cv 10 ./../../data/sampleCache30.pickle 30 2000 ./../../data/out.30_cv

'''
