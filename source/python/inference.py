import pystan
import numpy as np
import cPickle as pickle
import utils
from scipy.stats import norm
import scipy.linalg
import numpy.random
import csv
import math

def testLDA(dataload):
    '''  
      int<lower=2> K; // num topics
      int<lower=2> V; // num words
      int<lower=1> M; // num docs
      int<lower=1> N; // total word instances
      int<lower=1,upper=V> w[N]; // word n
      int<lower=1,upper=M> doc[N];  // doc ID for word n
      vector<lower=0>[K] alpha;     // topic prior
      vector<lower=0>[V] beta;      // word prior
    ''' 
    
    wordVec=[]
    docVec=[]
    docIdx=1
    for doc in dataload.catIdx.docsSet:
        
        #wordVec=np.concatenate((wordVec, doc.features_bag))
        wordVec=wordVec+doc.features_bag
        
        #docVec=np.concatenate((docVec, np.repeat([docIdx], len(doc.features_bag))))
        docVec=docVec+([docIdx]*len(doc.features_bag))
        docIdx+=1
        pass
    #wordVec=wordVec+1
    wordVec=[word+1 for word in wordVec]
    
    lda_data={'K': 10,
              'V': dataload.getVocabularySize(),
              'M': dataload.getDocsNo(),
              'N': len(docVec),
              'M': docIdx-1,
              'w': wordVec,
              'doc': docVec,             
              }   
    lda_data['alpha']=np.repeat([0.7], lda_data['K'])
    lda_data['beta']=np.repeat([1], lda_data['V'])
    
    fit = pystan.stan(file="./../lda.stan", data=lda_data,
                  iter=1000, chains=4, verbose=True)


def testHLDA(dataload):
    '''
      int<lower=2> K; // num topics
      int<lower=2> V; // num words
      int<lower=1> M; // num docs
      int<lower=1> N; // total word instances
      int<lower=1,upper=V> w[N]; // word n
      int<lower=1,upper=M> doc[N];  // doc ID for word n
      vector<lower=0>[K] alpha;     // topic prior
      vector<lower=0>[V] beta;      // word prior
    '''

    wordVec=[]
    docVec=[]
    docIdx=1
    catAssign = None
    for doc in dataload.catIdx.docsSet:

        #wordVec=np.concatenate((wordVec, doc.features_bag))
        wordVec=wordVec+doc.features_bag

        #docVec=np.concatenate((docVec, np.repeat([docIdx], len(doc.features_bag))))
        docVec=docVec+([docIdx]*len(doc.features_bag))
        docIdx+=1
        pass

        if catAssign == None:
            catAssign = np.array(doc.membership)
        else:
            catAssign = np.vstack((catAssign,doc.membership))
        #wordVec=wordVec+1

    catAssign = catAssign*2-1
    catNumber=dataload.getCatNo()
    Sigma =  np.cov(catAssign.T)
    LAMBDA=0.05
    regMat= np.zeros(shape=(catNumber,catNumber)) #Empty Adjacency Matrix
    np.fill_diagonal(regMat, LAMBDA)
    Sigma = Sigma + regMat

    print Sigma

    wordVec=[word+1 for word in wordVec]

    hlda_data={'K': 10,
              'V': dataload.getVocabularySize(),
              'M': dataload.getDocsNo(),
              'N': len(docVec),
              'M': docIdx-1,
              'L':catNumber,
              'w': wordVec,
              'doc': docVec,
              'catAssign':catAssign,
              'Sigma':Sigma,
              'alpha':1.0,
              }
    hlda_data['topicsprior']=np.repeat([0.7], hlda_data['K'])
    hlda_data['wordsprior']=np.repeat([1], hlda_data['V'])

    fit = pystan.stan(file="./../hlda.stan", data=hlda_data,
        iter=4000, chains=6, verbose=True,thin=6)
    
    fit_la = fit.extract(permuted=True)
    
    #print fit
    #fit.plot()
    resultsFile="./../../data/hlda.stan.res.pickle"
    pickle.dump(fit_la, open( resultsFile, "wb" ))
    
def testRes():
    import matplotlib.pyplot as plt
    resultsFile="./../../data/hlda.stan.res.pickle"
    fit_la=pickle.load(open( resultsFile, "rb" ))
    plt.plot(fit_la["lp__"])
    plt.show()
    z=1

def createCatMembershipVec(ldaVec,catNo):
    memb=[0]*catNo
    sumV=0
    countV=0
    for ldaVecElem in ldaVec:
        memb[ldaVecElem[0]]=ldaVecElem[1]
        sumV+=ldaVecElem[1]
        countV+=1
    sumV=(1-sumV)/(catNo-countV)
    for catId in range(0,catNo):
        if memb[catId]==0:
            memb[catId]=sumV
    return memb
    pass
    
def testFastLDA(dataload):
    from gensim import corpora, models, similarities
    #building corpus
    corpus=[]
    catAssign = None
    catAssignInt = None
    dictMapper=utils.Indexer()
    for doc in dataload.catIdx.docsSet:
        docRepr=[]
        for feat in doc.features.items():
            docRepr.append((dictMapper.addValue(feat[0]),feat[1]))
        corpus.append(docRepr)
        if catAssign == None:
            catAssign = np.array(doc.membership)
            catAssignInt = [doc.membership.astype(int)]
        else:
            catAssign = np.vstack((catAssign,doc.membership))
            catAssignInt.append(doc.membership.astype(int))
  
    catAssign = catAssign*2-1
    catNumber=dataload.getCatNo()
    Sigma =  np.cov(catAssign.T)
    LAMBDA=0.05
    regMat= np.zeros(shape=(catNumber,catNumber)) #Empty Adjacency Matrix
    np.fill_diagonal(regMat, LAMBDA)
    Sigma = Sigma + regMat
    empLabels = np.mean(catAssign,axis=0)

    print Sigma
    print empLabels
    
    outVal=numpy.random.multivariate_normal(empLabels,Sigma,30)
    print outVal
      
    print len(corpus)
    tfidf = models.TfidfModel(corpus)
    #index = similarities.SparseMatrixSimilarity(tfidf[corpus], num_features=12)
    #index = similarities.SparseMatrixSimilarity(tfidf[corpus])
    id2word=dictMapper.getInvertedMapping()
    num_topics=30
    lda = models.ldamodel.LdaModel(corpus=corpus, id2word=id2word, num_topics=num_topics, update_every=1, chunksize=10000, passes=30)
    doc2ldaTopicMembership=None
    for corpElem in corpus:
        membV=createCatMembershipVec(lda[corpElem],num_topics)
        if doc2ldaTopicMembership==None:
            doc2ldaTopicMembership=np.array(membV)
        else:
            doc2ldaTopicMembership=np.vstack((doc2ldaTopicMembership,np.array(membV)))
    
    
    hlda_data={'K': num_topics,
              'M': dataload.getDocsNo(),
              'L':catNumber,
              'theta': doc2ldaTopicMembership,
              'catAssign':catAssign,
              'catAssignInt':catAssignInt,
              'Sigma':Sigma,
              'empiricalLabels':empLabels,
              }
    

    fit = pystan.stan(file="./../mapping.stan", data=hlda_data,
        iter=6000, chains=2, verbose=True,thin=2)
    
    #fit_la = fit.extract(permuted=True)
    #fit_la_eta = fit_la["eta"]
    #fit_la_eta_fin=fit_la_eta[19,:,:] 
    fit_mean = fit.get_posterior_mean()
    print fit_mean
    print fit.model_pars
    import matplotlib.pyplot as plt
    #plt.plot(fit_la["lp__"])
    #plt.show()
    print fit_mean.shape
    
    etadim=catNumber*num_topics
    max_ll_idx=np.argmax(fit_mean[(etadim)+3,:])
    solution = {"eta" : fit_mean[0:etadim,max_ll_idx],
                #"intercept" : fit_mean[etadim:(etadim+catNumber),0],
                "mu": fit_mean[(etadim),max_ll_idx],
                "sigma": fit_mean[(etadim)+1,max_ll_idx],
                "sigmaerr": fit_mean[(etadim)+2,max_ll_idx],
                "loglik": fit_mean[(etadim)+3,max_ll_idx],
                }
    #solution["eta"].shape = (hlda_data["L"],hlda_data["K"])
    solution["eta"].shape = (hlda_data["K"],hlda_data["L"])
    solution["eta"]=solution["eta"].T

    ass=np.dot(solution["eta"],doc2ldaTopicMembership.T)
    
    np.savetxt("./../../data/testRes.csv", np.hstack([ass.T, catAssign]), delimiter=",")
    
    print "est:"
    print ass
    assprob=norm.cdf(ass,loc=0, scale=solution["sigmaerr"])
    assmod=ass-np.array([empLabels,]*hlda_data["M"]).T
    assmodprob=norm.cdf(assmod,loc=0, scale=solution["sigmaerr"])
    print "est.mod.prob:"
    print norm.cdf(assmod[:,1],loc=0, scale=solution["sigmaerr"])
    print "est.prob:"
    print norm.cdf(ass[:,1],loc=0, scale=solution["sigmaerr"])
    print "true:"
    print catAssign[1,:]
    print "est.mod.prob:"
    print norm.cdf(assmod[:,2],loc=0, scale=solution["sigmaerr"])
    print "est.prob:"
    print norm.cdf(ass[:,2],loc=0, scale=solution["sigmaerr"])
    print "true:"
    print catAssign[2,:]
    print "||est-MinusPlus|| = "+str(scipy.linalg.norm(ass.T-catAssign))
    print "2*#err[est] = "+str(np.sum(np.abs(np.sign(ass.T)-catAssign)))
    print "||est.prob-ZeroOne|| = "+str(scipy.linalg.norm(assprob.T-catAssignInt))
    print "2*#err[est.prob] = "+str(np.sum(np.abs(np.sign(assprob.T-0.5)-catAssign)))
    print "||est.mod-MinusPlus|| = "+str(scipy.linalg.norm(assmod.T-catAssign))
    print "2*#err[est.mod] = "+str(np.sum(np.abs(np.sign(assmod.T)-catAssign)))
    print "||est.mod.prob-ZeroOne|| = "+str(scipy.linalg.norm(assmodprob.T-catAssignInt))
    print "2*#err[est.mod.prob] = "+str(np.sum(np.abs(np.sign(assmodprob.T-0.5)-catAssign)))
    z=1
    #mapping topics to labels

def getStanModel():
    import os
    modPath="./../../data/HSLDA_probit.pickle"
    if os.path.isfile(modPath):
        return pickle.load(open(modPath, 'rb'))
    from pystan import StanModel

    model = StanModel(model_name="HSLDA_probit",file="./../mapping.stan")
    with open(modPath, 'wb') as f:
        pickle.dump(model, f)
    return model
    pass


class TrainParams:
    def __init__(self):
        self.ldaTopics=None
        self.ldaIt=30 
        self.LAMBDA=0.05
        self.mcmcIt=None
        self.mcmcChains=2
        self.mcmcThinning=2   
    
class TrainData:
    def __init__(self):
        self.corpus=[]
        self.catAssign = None
        self.catAssignInt = None
        self.dictMapper=utils.Indexer()
        self.catNumber=0
        pass
    
    def prepare(self,dataload):
        for doc in dataload.catIdx.docsSet:
            docRepr=[]
            for feat in doc.features.items():
                docRepr.append((self.dictMapper.addValue(feat[0]),feat[1]))
            self.corpus.append(docRepr)
            if self.catAssign == None:
                self.catAssign = np.array(doc.membership)
                self.catAssignInt = [doc.membership.astype(int)]
            else:
                self.catAssign = np.vstack((self.catAssign,doc.membership))
                self.catAssignInt.append(doc.membership.astype(int))
      
        self.catAssign = self.catAssign*2-1
        self.catNumber=dataload.getCatNo()
        
    def getDocsNo(self):
        return len(self.corpus)
    
    def extractDocSubset(self,idxVec):
        newTD=TrainData()
        newTD.dictMapper=self.dictMapper
        newTD.catNumber=self.catNumber
        newTD.catAssignInt=[]
        for idx in idxVec:
            newTD.catAssignInt.append(self.catAssignInt[idx])
            newTD.corpus.append(self.corpus[idx])
        newTD.catAssign=self.catAssign[idxVec,:]
        return newTD
        pass
               
def performFastLDACV(dataload, mcmcIt, ldaTopics, cvFold):
    td=TrainData()
    print "\t preparing representation"
    td.prepare(dataload)
    print "\t #documents: "+str(len(td.corpus))+", #categories: "+str(td.catNumber)+", #words: "+str(td.dictMapper.counter)

    tp=TrainParams()
    tp.mcmcIt=mcmcIt
    if ldaTopics==None:
        ldaTopics=int(math.ceil(td.catNumber*math.log(td.catNumber)))
    tp.ldaTopics=ldaTopics
    
    print "\t calculating Gaussian approximation"
    Sigma =  np.cov(td.catAssign.T)
    regMat= np.zeros(shape=(td.catNumber,td.catNumber)) #Empty Adjacency Matrix
    np.fill_diagonal(regMat, tp.LAMBDA)
    Sigma = Sigma + regMat
    empLabels = np.mean(td.catAssign,axis=0)

    print "\t performing CV"
    folding=utils.KFCrossVal(len(td.corpus),cvFold)
    results=-np.ones(td.catAssign.shape)
    for train, test in folding:
        trainTD=td.extractDocSubset(train)
        model=trainFastLDA(trainTD,tp,empLabels, Sigma)
        testTD=td.extractDocSubset(test)
        res=applyFastLDA(model,testTD)
        #print results.shape, results[test,:].shape, res.shape  
        results[test,:]=res.T
        pass
    printDiag(results.T,td.catAssign,td.catAssignInt)
    pass     

def applyFastLDA(model,newTD):
    doc2ldaTopicMembership=None
    print "\t applying LDA:"
    for corpElem in newTD.corpus:
        membV=createCatMembershipVec(model["lda"][corpElem],model["ldaTopics"])
        if doc2ldaTopicMembership==None:
            doc2ldaTopicMembership=np.array(membV)
        else:
            doc2ldaTopicMembership=np.vstack((doc2ldaTopicMembership,np.array(membV)))
    ass=np.dot(model["eta"],doc2ldaTopicMembership.T)
    assmod=ass-np.array([model["empLabels"],]*newTD.getDocsNo()).T

    assprob=norm.cdf(assmod,loc=0, scale=model["sigmaerr"])
    return assprob
    pass

#def performFastLDAHoldout(trainIdx,testIdx,trainData,trainParams,empLabels, Sigma):
#    trainTD=trainData.extractDocSubset(trainIdx)
#    testTD=trainData.extractDocSubset(testIdx)
    
#    model=trainFastLDA(trainTD,trainParams,empLabels, Sigma)
#    pass

def trainFastLDA(trainData,trainParams,empLabels, Sigma):
    from gensim import models

    print "\t performing LDA (#topics="+str(trainParams.ldaTopics)+")"
        
    id2word=trainData.dictMapper.getInvertedMapping()
    lda = models.ldamodel.LdaModel(corpus=trainData.corpus, id2word=id2word, num_topics=trainParams.ldaTopics, update_every=1, chunksize=10000, passes=30)
    doc2ldaTopicMembership=None
    print "\t applying LDA:"
    for corpElem in trainData.corpus:
        membV=createCatMembershipVec(lda[corpElem],trainParams.ldaTopics)
        if doc2ldaTopicMembership==None:
            doc2ldaTopicMembership=np.array(membV)
        else:
            doc2ldaTopicMembership=np.vstack((doc2ldaTopicMembership,np.array(membV)))
    
    
    hlda_data={'K': trainParams.ldaTopics,
              'M': trainData.getDocsNo(),
              'L':trainData.catNumber,
              'theta': doc2ldaTopicMembership,
              'catAssign':trainData.catAssign,
              'catAssignInt':trainData.catAssignInt,
              'Sigma':Sigma,
              'empiricalLabels':empLabels,
              }
    
    print "\t performing mapping inference (#iterations="+str(trainParams.mcmcIt)+", #chains="+str(trainParams.mcmcChains)+", thinning="+str(trainParams.mcmcThinning)+"):"
    fit = getStanModel().sampling(data=hlda_data,
        iter=trainParams.mcmcIt, chains=trainParams.mcmcChains, verbose=True,thin=trainParams.mcmcThinning)
    
    print "\t extracting solution"
    fit_mean = fit.get_posterior_mean()
    
    etadim=trainData.catNumber*trainParams.ldaTopics
    max_ll_idx=np.argmax(fit_mean[(etadim)+3,:])
    solution = {"eta" : fit_mean[0:etadim,max_ll_idx],
                #"intercept" : fit_mean[etadim:(etadim+catNumber),0],
                "mu": fit_mean[(etadim),max_ll_idx],
                "sigma": fit_mean[(etadim)+1,max_ll_idx],
                "sigmaerr": fit_mean[(etadim)+2,max_ll_idx],
                "loglik": fit_mean[(etadim)+3,max_ll_idx],
                }
    #solution["eta"].shape = (hlda_data["L"],hlda_data["K"])
    solution["eta"].shape = (hlda_data["K"],hlda_data["L"])
    solution["eta"]=solution["eta"].T
    model={"lda":lda,
           "wordDict":trainData.dictMapper,
           "ldaTopics":trainParams.ldaTopics,
           "eta":solution["eta"],
           "sigmaerr":solution["sigmaerr"],
           "empLabels":empLabels
           }
    return model
    pass

def printDiag(results,catAssign,catAssignInt):
    classSize=results.shape[0]*results.shape[1]
    rse=scipy.linalg.norm(results.T-catAssignInt)
    mse=rse*rse/(classSize)
    misclass=np.sum(np.abs(np.sign(results.T-0.5)-catAssign))
    print "\t\t MSE = "+str(mse)
    print "\t\t %misclassified = "+str(misclass/classSize)
    y_predicted=np.asarray(np.sign(results.T-0.5)).reshape(-1)    
    y_true=np.asarray(catAssign).reshape(-1)
    score_res=utils.Score(y_true, y_predicted)
    print "\t\t precision: "+str(score_res[0])
    print "\t\t recall: "+str(score_res[1])
    print "\t\t F-measure: "+str(score_res[2])
    print "\t\t support: "+str(score_res[3])

    pass

def performFastLDA(dataload, mcmcIt, ldaTopics, output_file):
    from gensim import models
    #building corpus
    td=TrainData()
    print "\t preparing representation"
    td.prepare(dataload)
    print "\t #documents: "+str(len(td.corpus))+", #categories: "+str(td.catNumber)+", #words: "+str(td.dictMapper.counter)
    
    print "\t calculating Gaussian approximation"
    Sigma =  np.cov(td.catAssign.T)
    LAMBDA=0.05
    regMat= np.zeros(shape=(td.catNumber,td.catNumber)) #Empty Adjacency Matrix
    np.fill_diagonal(regMat, LAMBDA)
    Sigma = Sigma + regMat
    empLabels = np.mean(td.catAssign,axis=0)

    if ldaTopics==None:
        ldaTopics=int(math.ceil(td.catNumber*math.log(td.catNumber)))
        
    print "\t performing LDA (#topics="+str(ldaTopics)+")"
        
    id2word=td.dictMapper.getInvertedMapping()
    lda = models.ldamodel.LdaModel(corpus=td.corpus, id2word=id2word, num_topics=ldaTopics, update_every=1, chunksize=10000, passes=30)
    doc2ldaTopicMembership=None
    print "\t applying LDA:"
    for corpElem in td.corpus:
        membV=createCatMembershipVec(lda[corpElem],ldaTopics)
        if doc2ldaTopicMembership==None:
            doc2ldaTopicMembership=np.array(membV)
        else:
            doc2ldaTopicMembership=np.vstack((doc2ldaTopicMembership,np.array(membV)))
    
    
    hlda_data={'K': ldaTopics,
              'M': dataload.getDocsNo(),
              'L':td.catNumber,
              'theta': doc2ldaTopicMembership,
              'catAssign':td.catAssign,
              'catAssignInt':td.catAssignInt,
              'Sigma':Sigma,
              'empiricalLabels':empLabels,
              }
    
    mcmcChains=2
    mcmcThinning=2
    print "\t performing mapping inference (#iterations="+str(mcmcIt)+", #chains="+str(mcmcChains)+", thinning="+str(mcmcThinning)+"):"
    fit = getStanModel().sampling(data=hlda_data,
        iter=mcmcIt, chains=mcmcChains, verbose=True,thin=mcmcThinning)
    
    print "\t extracting solution"
    fit_mean = fit.get_posterior_mean()
    
    etadim=td.catNumber*ldaTopics
    max_ll_idx=np.argmax(fit_mean[(etadim)+3,:])
    solution = {"eta" : fit_mean[0:etadim,max_ll_idx],
                #"intercept" : fit_mean[etadim:(etadim+catNumber),0],
                "mu": fit_mean[(etadim),max_ll_idx],
                "sigma": fit_mean[(etadim)+1,max_ll_idx],
                "sigmaerr": fit_mean[(etadim)+2,max_ll_idx],
                "loglik": fit_mean[(etadim)+3,max_ll_idx],
                }
    #solution["eta"].shape = (hlda_data["L"],hlda_data["K"])
    solution["eta"].shape = (hlda_data["K"],hlda_data["L"])
    solution["eta"]=solution["eta"].T

    print "\t calculating performance (train set):"
    ass=np.dot(solution["eta"],doc2ldaTopicMembership.T)
    assmod=ass-np.array([empLabels,]*hlda_data["M"]).T

    assprob=norm.cdf(assmod,loc=0, scale=solution["sigmaerr"])
    classSize=hlda_data["M"]*hlda_data["L"]
    rse=scipy.linalg.norm(assprob.T-td.catAssignInt)
    mse=rse*rse/(classSize)
    misclass=np.sum(np.abs(np.sign(assprob.T-0.5)-td.catAssign))
    print "\t\t MSE = "+str(mse)
    print "\t\t %misclassified = "+str(misclass/classSize)
    y_predicted=np.asarray(np.sign(assprob.T-0.5)).reshape(-1)    
    y_true=np.asarray(td.catAssign).reshape(-1)
    score_res=utils.Score(y_true, y_predicted)
    print "\t\t precision: "+str(score_res[0])
    print "\t\t recall: "+str(score_res[1])
    print "\t\t F-measure: "+str(score_res[2])
    print "\t\t support: "+str(score_res[3])
    model={"lda":lda,
           "wordDict":td.dictMapper,
           "ldaTopics":ldaTopics,
           "eta":solution["eta"],
           "sigmaerr":solution["sigmaerr"],
           "empLabels":empLabels
           }
    if output_file!=None:
        pickle.dump( model, open( output_file, "wb" ) )
        print "Resulting model written in "+output_file+"."
    return model
    
    #np.savetxt("./../../data/testRes.csv", np.hstack([ass.T, td.catAssign]), delimiter=",")
        
    
        
    
    
    