import operator
import numpy as np

def mapDict(redSel):
    objMapD = dict()
    idMapD = dict()
    c = 0
    for p in redSel:
        objMapD.update({p:c})
        idMapD.update({p.catId:c})
        c += 1
    return objMapD, idMapD


def mBuild(redSel, objMapD, idMapD):
    l = len(objMapD)
    adjMat= np.zeros(shape=(l,l)) #Empty Adjacency Matrix
    np.fill_diagonal(adjMat, 1)
    #print adjMat


    for obj, indx in objMapD.iteritems():
        pars = obj.parents
        tmp_MapIDs = []
        for p in pars:
            #try:
            pId = idMapD[p.catId]
            tmp_MapIDs.append(pId)
            #except:
            #    pass
        adjMat[indx,tmp_MapIDs] = 1
    #print adjMat
    return adjMat

def mDot(adjMat):
    zero_check = 1

    base = adjMat
    tmp_Mat = adjMat
    while zero_check != 0:
        tmp_Mat = base
        base = np.dot(adjMat, base)
        base = np.sign(base)
        zero_check = np.sum(np.abs(tmp_Mat-base))
        #print zero_check

    return base

def mainMD(redSel):
    objMapD, idMapD = mapDict(redSel)
    adjMat = mBuild(redSel, objMapD, idMapD)
    lastMat = mDot(adjMat)
    return adjMat, lastMat, objMapD