from model import *

import os
from sampling import *
from matrix_gen import mainMD
import re
from utils import Indexer


class DataLoader:
    def __init__(self):
        #self.path=path
        self.catIdx=CategoryIndex()
        self.docsFile=None
        patt=r"^((\d+, )*\d+) ((\d+:\d+ )*\d+:\d+)$"
        self.docsPatt=re.compile(patt)
        self.wordDict=Indexer()

    def getDocsNo(self):
        return len(self.catIdx.docsSet)
    
    def getVocabularySize(self):
        return self.wordDict.getNumber()
    
    def getCatNo(self):
        return len(self.catIdx.catId2Obj.keys())
        
    def perform(self):
        print "Loading hierarchy of categories, current dir: "+os.getcwd()
        self.loadHierarchy()
        print "Indexing the corpora, current dir: "+os.getcwd()
        self.loadDocs()

    def loadHierarchy(self):
        
    
        with open('./../../data/hierarchy.txt') as f:
            lines = f.read().splitlines()

        for line in lines:
            catPair=line.split(" ")
            self.catIdx.addPair(int(catPair[0]), int(catPair[1]))
    
    def loadDocs(self):

        #ttm="314523, 165538, 76255, 335416, 416827 1250536:1 1744638:1 298526:1 1568238:1 77076:1 1327541:1 1416569:1 1586020:1 1700948:1 652007:1 1815811:1 488832:2 572742:1 1137332:2 1473311:1 513820:1 1210088:1 423304:1 1357328:1 1673413:1 214525:1 1733491:1 1313723:1 1540893:5 1465710:1 2021871:1 1936077:1 513790:1 660115:1 1287792:3 481092:1 843169:2 572336:1 460447:1 97802:1 1397861:3 1284496:1 1875794:1 1972806:1 1457483:1 227881:3 410359:1 265208:7 985823:1 331158:1 85847:1 1402995:1 1710579:1 102853:1 1274838:1 1105861:2 892257:1 951474:1 1004657:1 234275:1 830416:1 563647:3 1564710:4 1156150:1 970168:2 592526:3 550210:1 453755:1 705700:1 1470603:1 903090:1 614785:2 1151929:1 1303404:1 2070888:1 1837036:1 1360760:1 1611236:1 955673:1 49164:2 704224:2 1137133:1 1298082:1 977746:1 9364:1 1267431:1"
        
        trainFile = open( './../../data/train.csv', 'r' )
        trainFile.readline() # skip the headers
        pos=trainFile.tell()
        line=trainFile.readline()
        
        while line!="":
            match = re.search(self.docsPatt, line.strip())
            labels= [int(x) for x in match.group(1).split(", ")]
            #features=match.group(3).split(" ")
            document=Document(pos)
            for cat in labels:
                self.catIdx.addDocument(document, cat)
            pos=trainFile.tell()
            line=trainFile.readline()
            pass
        self.docsFile=trainFile
    
    def restrictToSample(self, iterableOfCat):
        newDataLoader=DataLoader()
        newDataLoader.docsFile=self.docsFile
        newDataLoader.catIdx=self.catIdx.restrictToSample(iterableOfCat)
        return newDataLoader
        pass
    
    def loadDocsFeatures(self):
        for doc in self.catIdx.docsSet:
            '''getting features'''
            self.docsFile.seek(doc.filePos)
            line=self.docsFile.readline()
            match = re.search(self.docsPatt, line.strip())
            #labels= [int(x) for x in match.group(1).split(", ")]
            features=match.group(3).split(" ")
            #already have labels, now we do extract words
            docFeatures={}
            for fval in features:
                splVal=fval.split(":")
                docFeatures[splVal[0]]=int(splVal[1])
            doc.features=docFeatures
        pass
    
    def buildDictionary(self):
        for doc in self.catIdx.docsSet:
            '''bag of words'''
            doc.features_bag=self.wordDict.add(doc.features.keys())
            pass
        pass
    
    def finalizeLoad(self):
        #self.docsFile.close()
        self.docsFile=None
